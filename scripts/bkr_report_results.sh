#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

if [[ -z "${BEAKER_JOBIDS:-}" ]]; then
    echo "FAIL: BEAKER_JOBIDS is not set"
    exit 0
fi

if [[ -z "${DW_CHECKOUT}" ]]; then
    echo "FAIL: DW_CHECKOUT is not set"
    exit 1
fi

JOB_NOTIFY=${JOB_NOTIFY:-${JOB_OWNER}}

echo "Getting results from beaker jobs: ${BEAKER_JOBIDS}"
# shellcheck disable=SC2086 # we want word splitting on BEAKER_JOBIDS
for jobid in ${BEAKER_JOBIDS}; do
    bkr job-results --prettyxml "${jobid}" > "job_${jobid}.xml"
done

echo "reporting results of beaker jobs ${BEAKER_JOBIDS}"
compose=$(cat job_*.xml | sed -n 's/.*distro="\([a-zA-Z0-9.-]\+\)".*/\1/p' | head -1)
if [[ -z "${compose}" ]]; then
    echo "FAIL: couldn't find beaker compose used by job: ${BEAKER_JOBIDS}"
    exit 1
fi

if [[ -n "${TEST_PACKAGE_NVR:-}" ]]; then
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
        echo "FAIL: TEST_PACKAGE_NVR is set as ${TEST_PACKAGE_NVR}, but TEST_SOURCE_PACKAGE_NVR is not set."
        exit 1
    fi
    # it is expected if TEST_PACKAGE_NVR is set that TEST_SOURCE_PACKAGE_NVR is also set
    src_pkg_nvr="${TEST_SOURCE_PACKAGE_NVR}"
    pkg_nvr="${TEST_PACKAGE_NVR}"
else
    src_pkg_nvr=$(find_compose_pkg -c "${compose}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
    if [[ -z "${src_pkg_nvr}" ]]; then
        echo "FAIL: couldn't find nvr for ${TEST_PACKAGE_NAME:?} on compose ${compose}"
        exit 1
    fi
    # set the packge nvr based on nvr of package source nvr
    # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
    pkg_nvr=$(echo "${src_pkg_nvr}" | sed "s/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}/")
fi
# allow DW_CHECKOUT to contain command line execution
DW_CHECKOUT=$(eval echo "${DW_CHECKOUT}")
contacts=()
for contact in ${JOB_NOTIFY}; do
    contacts+=(--contact "${contact}"@redhat.com)
done

index=0
merge_results=()
for jobxml in job_*.xml; do
    # some job submiter like workflow tomorrow adds the fetch url as task name, let's remove it, before submiting to DW
    sed -i -E 's/(.*task name.*)(http.*#)/\1/' "${jobxml}"
    sed -i -E 's/(.*task name.*)(git.*#)/\1/' "${jobxml}"
    kcidb_tool create --source beaker --src-nvr "${src_pkg_nvr}" --nvr "${pkg_nvr}" "${contacts[@]}" --checkout-origin "${TEST_JOB_NAME:?}" --tests-origin "${TEST_JOB_NAME:?}" -i "${jobxml}" -c "\"${DW_CHECKOUT}\"" -o "output${index}.json" --tests-provisioner-url "${CI_JOB_URL:?}"
    merge_results+=(-r "output${index}.json")
    index=$(( index+1 ))
done
kcidb_tool merge "${merge_results[@]}" -o output.json
kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem
