#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

. scripts/bkr_common.sh

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')

git clone --depth 1 "${ZSUBMIT_REPO:?}" zsubmit-repo
cp -r zsubmit-repo/Sustaining .
rm -rf zsubmit-repo
pushd ./Sustaining || exit 1

pip3 install -r 'requirements.txt'

if [[ -z "${BKR_COMPOSE_TAGS}" ]]; then
    echo "Fail: BKR_COMPOSE_TAGS is not set."
    exit 1
fi

# shellcheck disable=SC2086 # Double quote to prevent globbing
tags="--tag ${BKR_COMPOSE_TAGS// / --tag }"
# shellcheck disable=SC2086 # Double quote to prevent globbing
bkr distros-list --name "${BKR_COMPOSE:?}" ${tags} --limit 1 | tee compose.txt
DISTRO=$(grep "Name:" compose.txt | awk '{print$2}')
# it seems some jobs using zsubmit relies on DISTRO variable
export DISTRO
if [[ -n "${TEST_PACKAGE_NVR:-}" ]]; then
    pkg_nvr="${TEST_PACKAGE_NVR}"
else
    pkg_nvr=$(find_compose_pkg -c "${DISTRO}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
fi
if [[ -z "${pkg_nvr}" ]]; then
    echo "FAIL: couldn't find nvr for ${TEST_PACKAGE_NAME} on compose ${DISTRO}"
    exit 1
fi

if [[ -v ZSUBMIT_PARAMS ]]; then
    # allow ZSUBMIT_PARAMS to contain command line execution
    ZSUBMIT_PARAMS=$(eval echo "${ZSUBMIT_PARAMS}")
    # shellcheck disable=SC2312
    readarray -t -d ' ' zsubmit_params < <(echo "${ZSUBMIT_PARAMS}" | tr -d '\n')
    zsubmit_params+=(--nvr "${pkg_nvr}")
    if [[ "${TEST_ARCH}" != "allarches" ]]; then
        zsubmit_params+=(--arch "${TEST_ARCH}")
    fi
    if [[ -v ZSUBMIT_TIER ]]; then
        zsubmit_params+=(--tier "${ZSUBMIT_TIER}")
    fi
    python3 ZSubmit.py "${zsubmit_params[@]}" --idf bkr_ids.txt
fi

if [[ -v ZADDON_PARAMS ]]; then
    # shellcheck disable=SC2312
    readarray -t -d ' ' zaddon_params < <(echo "${ZADDON_PARAMS}" | tr -d '\n')
    zaddon_params+=(--nvr "${pkg_nvr}")
    if [[ -v ZADDON_TIER ]]; then
        zaddon_params+=(--tier "${ZADDON_TIER}")
    fi
    python3 ZAddon.py "${zaddon_params[@]}" --idf bkr_ids.txt
fi

if ! grep -q '[^[:space:]]' bkr_ids.txt; then
    echo "No job created, skipping"
    exit 0
fi

BEAKER_JOBIDS=$(cat bkr_ids.txt)

popd || exit 1

{
    echo BEAKER_JOBIDS="${BEAKER_JOBIDS}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env

if [[ -z "${BEAKER_JOBIDS}" ]]; then
    echo "INFO: No job submitted to beaker."
    exit 0
fi

wait_jobs
