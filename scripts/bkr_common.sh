#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

wait_jobs() {
    if [[ -z "${BEAKER_JOBIDS:-}" ]]; then
        echo "FAIL: BEAKER_JOBIDS is not set"
        exit 0
    fi
    echo "Waiting for beaker jobs to finish: ${BEAKER_JOBIDS}"

    TEST_JOB_TIMEOUT_HOURS=${TEST_JOB_TIMEOUT_HOURS:-24}

    # wait for 24hrs for jobs to complete
    wait_start=$(date +%s)
    max_time=$(( TEST_JOB_TIMEOUT_HOURS*3600 ))
    # in case of failures connecting to beaker `bkr job-watch` might fail
    # we should retry it and continue waiting for up to 24hrs since original
    # start time.
    # don't fail script if bkr job-watch doesn't return 0 as this is normal if some task doesn't succeed
    set +e
    # shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
    while [[ $(( $(date +%s) - max_time )) -lt "${wait_start}" ]]; do
        # shellcheck disable=SC2086 # we want word splitting on BEAKER_JOBIDS
        timeout $(( $(date +%s) - max_time )) bkr job-watch ${BEAKER_JOBIDS}
        if [[ $? == 124 ]]; then
            echo "WARN: tests took over ${TEST_JOB_TIMEOUT_HOURS} hours to run, give up waiting for completion..."
            # shellcheck disable=SC2086 # we want word splitting on BEAKER_JOBIDS
            bkr job-cancel ${BEAKER_JOBIDS}
            break
        fi

        all_jobs_ended=1
        # shellcheck disable=SC2086 # we want word splitting on BEAKER_JOBIDS
        for jobid in ${BEAKER_JOBIDS}; do
            bkr job-results --prettyxml "${jobid}" > "job_${jobid}.xml"
            # Make sure the job actually ended
            job_status=$(sed -n 's/.*status="\([a-zA-Z]\+\)".*/\1/p' < "job_${jobid}.xml" | tail -1)
            if [[ "${job_status}" != "Completed" ]] &&
               [[ "${job_status}" != "Aborted" ]] &&
               [[ "${job_status}" != "Cancelled" ]]; then
                    all_jobs_ended=0
                    break
            fi
        done
        if [[ "${all_jobs_ended}" -eq 1 ]]; then
            break
        fi
    done

    set -xe
}
