#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
tft_params=("--compose ${TFT_COMPOSE}" "--arch ${TEST_ARCH}"
            "--git-url ${TFT_PLAN_URL}" "--plan ${TFT_PLAN}")
if [[ -n "${TFT_CONTEXT:-}" ]]; then
    tft_params+=("--context ${TFT_CONTEXT}")
fi

# shellcheck disable=SC2086,SC2048 # Double quote to prevent globbing - Use "$@" (with quotes) to prevent whitespace problems
testing-farm request --no-wait --kickstart "kernel-options=enforcing=0" --kickstart "kernel-options-post=enforcing=0 rd.emergency=reboot" \
    ${tft_params[*]} | tee request_output.txt
TFT_REQUEST_ID=$(grep "api" request_output.txt | grep -o '[^/]*$')
if [[ -z "${TFT_REQUEST_ID}" ]]; then
    echo "FAIL: Couldn't submit testing-farm request."
    exit 1
fi

{
    echo TFT_REQUEST_ID="${TFT_REQUEST_ID}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:?}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env

# don't fail pipeline if a command fails
set +e
# wait for 24hrs for jobs to complete
wait_start=$(date +%s)
max_time=$(( 24*3600 ))
# in case of failures connecting to beaker `bkr job-watch` might fail
# we should retry it and continue waiting for up to 24hrs since original
# start time.
# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
while [[ $(( $(date +%s) - max_time )) -lt "${wait_start}" ]]; do
    timeout $(( $(date +%s) - max_time )) testing-farm watch --id "${TFT_REQUEST_ID}"
    if [[ $? == 124 ]]; then
        echo "WARN: tests took over 24hr to run, give up waiting for completion..."
        testing-farm cancel "${TFT_REQUEST_ID}"
        break
    fi

    # make sure watch returned, because request completed and not due some infra problem
    curl --retry 50 "${TFT_API_URL:?}"/requests/"${TFT_REQUEST_ID}" > request_status.json
    state=$(jq -r ".state" < request_status.json)
    if [[ "${state}" == "complete" ]] ||
       [[ "${state}" == "error" ]]; then
        break
    fi
done
set -e
if [[ "${state}" == "error" ]]; then
    reason=$(jq -r '.result.summary' < request_status.json)
    echo "FAIL: testing farm failed due to: ${reason}"
    exit 1
fi
