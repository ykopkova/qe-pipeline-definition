#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

set -x
# don't fail pipeline if a command fails
set +e
git clone --depth 1 "${STBKR_TESTS_CONF_REPO:?}" kernel
pushd kernel/storage/misc/toolbox || exit 1
# remove the / that are passed with TEST_PACKAGE_NAME_ARCH
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
if [[ "${TEST_PACKAGE_NAME}" == "kernel" ]]; then
    DEFAULT_KERNEL=1
    DEBUG_KERNEL=0
elif [[ "${TEST_PACKAGE_NAME}" == "kernel-debug" ]]; then
    DEFAULT_KERNEL=0
    DEBUG_KERNEL=1
else
    echo "FAIL: unsupported kernel package ${TEST_PACKAGE_NAME}"
    exit 1
fi
stbkr --default-kernel "${DEFAULT_KERNEL}" --debug-kernel "${DEBUG_KERNEL}" -r "${BKR_COMPOSE:?}" --dist_tag "${BKR_COMPOSE_TAG:?}"  -c test_conf/"${CONFIG:?}" -g "${STBKR_GROUP:?}" -a "${TEST_ARCH}" --job-owner "${JOB_OWNER:?}" > bkr_job.txt
JOB_IDS=$(sed -e "s/.*\['\(.*\)'\]/\1/g" < bkr_job.txt)
popd || exit 1
if [[ -z "${JOB_IDS}" ]]; then
    echo "FAIL: Couldn't submit beaker job."
    exit 1
fi

{
    echo BEAKER_JOBIDS="${JOB_IDS}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:?}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env
