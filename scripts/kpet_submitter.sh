#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

set -x
# don't fail pipeline if a command fails
set +e
git clone --depth 1 "${KPET_DB_URL:?}" kpet-db
# remove the / that are passed with TEST_PACKAGE_NAME_ARCH
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')

if [[ -z "${KPET_DESCRIPTION}" ]]; then
    KPET_DESCRIPTION="OS: ${KPET_TREE:?}, arch: ${TEST_ARCH}, test plan: ${KPET_SET:?}"
fi

# allow KPET_DESCRIPTION to contain command line execution
KPET_DESCRIPTION=$(eval echo "${KPET_DESCRIPTION}")
kpet --db kpet-db run generate --tree "${KPET_TREE:?}" --arch "${TEST_ARCH}" --set "${KPET_SET:?}" --description "${KPET_DESCRIPTION}" --output beaker.xml
bkr job-submit beaker.xml > bkr_job.txt
JOB_IDS=$(sed -e "s/.*\['\(.*\)'\]/\1/g" < bkr_job.txt)
if [[ -z "${JOB_IDS}" ]]; then
    echo "FAIL: Couldn't submit beaker job."
    exit 1
fi

{
    echo BEAKER_JOBIDS="${JOB_IDS}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:?}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env
