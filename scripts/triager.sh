#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x
if [[ ! -f ./output.json ]]; then
    echo "No result to triage, skipping"
    exit 0
fi
python3 -m cki.triager --triage-type offline --input-file ./output.json --output-file ./output_triaged.json
