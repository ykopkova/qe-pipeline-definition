#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

if [[ "${INFRA_ERROR:-}" == "1" ]]; then
cat << EOF > slack_message.txt
Error on CI pipeline (${TEST_JOB_NAME:-}): ${CI_SERVER_URL:-}/${CI_PROJECT_NAMESPACE:-}/${CI_PROJECT_NAME:-}/pipelines/${CI_PIPELINE_ID:-}

@bgoncalv please review...
EOF
else
    if [[ ! -f output_triaged.json ]]; then
        echo "No result to notify, skipping"
        exit 0
    fi

    jinja_renderer -t templates/notify_slack.j2 -i output_triaged.json -o slack_message.txt
fi

if [[ ! -f slack_message.txt ]]; then
    echo "FAIL: couldn't create slack message"
    exit 1
fi

cat slack_message.txt

# only send message if needs to notify someone
if grep "please review" slack_message.txt; then
    send_slack_notification --message-file slack_message.txt
fi
