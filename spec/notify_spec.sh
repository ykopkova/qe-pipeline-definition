#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'notify'
    startup() {
        touch output_triaged.json
    }

    cleanup() {
        rm -f slack_message.txt
        rm -rf output_triaged.json
    }
    Before 'startup'
    After 'cleanup'

    Mock jinja_renderer
        echo "@test please review" > slack_message.txt
    End

    Mock send_slack_notification
    End

    It 'can notify'
        When run script scripts/notify.sh
        The status should be success
        The stdout should include "@test please review"
        The stderr should include "jinja_renderer -t templates/notify_slack.j2 -i output_triaged.json -o slack_message.txt"
        The stderr should include "send_slack_notification --message-file slack_message.txt"
    End
End
