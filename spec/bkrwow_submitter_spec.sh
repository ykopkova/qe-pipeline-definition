#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'bkrwow_submitter'
    cleanup() {
        rm -rf test_variables.env
    }
    AfterEach 'cleanup'

    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export WOW_TASKFILE=mock-taskfile
    export JOB_OWNER=test
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export WOW_WHITEBOARD='test $(cmd)'
    export BKR_COMPOSE="mock_compose"
    export BKR_COMPOSE_TAGS='mock-tag1 mock-tag2'

    export bkr_distro_output="Name: mocked_compose"
    export bkr_wow_output="TJ#123456"

    Mock cmd
        echo "mycmd"
    End

    Mock bkr
        if [[ "$1" == "distros-list" ]]; then
            echo "${bkr_distro_output}"
        elif [[ "$1" == "workflow-tomorrow" ]]; then
            echo "${bkr_wow_output}"
        else
            echo "fail, unsupported bkr command $1"
            exit 1
        fi
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0-mock.el10.x86_64"
    End

    It 'can submit job'
        When run script scripts/bkrwow_submitter.sh
        The status should be success
        The stdout should include "${bkr_distro_output}"
        The stdout should include "${bkr_wow_output}"
        The stderr should include "bkr distros-list --name mock_compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The stderr should include "bkr workflow-tomorrow --distro mocked_compose --arch x86_64 --ks-meta=redhat_ca_cert --exact --task-file mock-taskfile --job-owner=test --whiteboard 'test mycmd - x86_64'"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
End
