#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'triager'
    startup() {
        touch ./output.json
    }

    cleanup() {
        rm -rf ./output.json
    }
    Before 'startup'
    After 'cleanup'
    Mock python3
        echo "python3 $*"
    End

    export output="python3 -m cki.triager --triage-type offline --input-file ./output.json --output-file ./output_triaged.json"

    It 'can triage'
        When run script scripts/triager.sh
        The status should be success
        The stdout should include "${output}"
        The stderr should include "${output}"
    End
End
