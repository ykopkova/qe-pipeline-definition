#!/usr/bin/bash

# shellcheck disable=SC2317 # Command appears to be unreachable

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'kpet_submitter'
    cleanup() {
        rm -rf test_variables.env
    }
    AfterEach 'cleanup'

    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export KPET_DB_URL=mocked_kpet_db_url
    export KPET_TREE=tree-mock
    export KPET_SET=set-mock
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export KPET_DESCRIPTION='date: $(date)'
    export mocked_date="Wed May 22 11:22:03 UTC 2024"

    export bkr_output="J:123456"

    Mock git
        echo "git $*"
    End

    Mock kpet
        exit 0
    End

    Mock bkr
        echo "${bkr_output}"
    End

    Mock date
        echo "${mocked_date}"
    End

    It 'can submit job'
        When run script scripts/kpet_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mocked_kpet_db_url kpet-db"
        The stderr should include "kpet --db kpet-db run generate --tree tree-mock --arch x86_64 --set set-mock --description 'date: ${mocked_date}' --output beaker.xml"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=${bkr_output}"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
End
