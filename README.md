# Kernel QE CI pipelines

## Pipelines

These are pipelines owned by kernel-qe team to run tests for kernel and userspace packages.

Each pipeline trigger can contain multiple jobs with tests from different person/team.
All jobs from the same trigger should report to same [DataWarehouse checkout](https://datawarehouse.cki-project.org)

